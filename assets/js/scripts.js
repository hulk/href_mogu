
jQuery(document).ready(function() {

    $('.page-container form').submit(function(){
        var video = $(this).find('.video').val();
        var picture = $(this).find('.picture').val();
        if(video == '') {
            $(this).find('.error').fadeOut('fast', function(){
                $(this).css('top', '27px');
            });
            $(this).find('.error').fadeIn('fast', function(){
                $(this).parent().find('.video').focus();
            });
            return false;
        }
        if(picture == '') {
            $(this).find('.error').fadeOut('fast', function(){
                $(this).css('top', '96px');
            });
            $(this).find('.error').fadeIn('fast', function(){
                $(this).parent().find('.picture').focus();
            });
            return false;
        }
    });

    $('.page-container form .video, .page-container form .picture').keyup(function(){
        $(this).parent().find('.error').fadeOut('fast');
    });

	$('[name="video"]').change(function(){
		$('.video').val('Video: ' + $(this).val());
	});
	
	$('[name="video_false"]').change(function(){
		$('.video_false').val('VideoF: ' + $(this).val());
	});
	
	$('[name="picture"]').change(function(){
		$('.picture').val('Picture: ' + $(this).val());
	});
});
